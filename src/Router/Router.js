import React, {Component} from 'react'
import { Route, NativeRouter, Switch, Redirect} from 'react-router-native'
import Login from '../Pages/Login';
import Main from '../Pages/Main';
import Register from '../Pages/Register';
import Profile from '../Pages/Profile';
import History from '../Pages/History';
import AddWallet from '../Pages/AddWallet';
import YourWallet from '../Pages/YourWallet';
import EditWallet from '../Pages/EditWallet';
import Income from '../Pages/Income';
import Expense from '../Pages/Expense';
import CategoryIncome from '../Pages/CategoryIncome';
import CategoryExpense from '../Pages/CategoryExpense';

class Router extends Component {
    render() {
        return (
           <NativeRouter>
               <Switch>
                   <Route exact path="/login" component={Login} />
                   <Route exact path="/main" component={Main} />
                   <Route exact path="/register" component={Register}/>
                   <Route exact path="/profile" component={Profile} />
                   <Route exact path="/history" component={History} />
                   <Route exact path="/addwallet" component={AddWallet} />
                   <Route exact path="/yourwallet" component={YourWallet} />
                   <Route exact path="/editwallet" component={EditWallet} />
                   <Route exact path="/income" component={Income} />
                   <Route exact path="/expense" component={Expense} />
                   <Route exact path="/categoryincome" component={CategoryIncome} />
                   <Route exact path="/categoryexpense" component={CategoryExpense} />
                   <Redirect to="/login"/>
               </Switch>
           </NativeRouter>
        )
    }
}
export default Router
