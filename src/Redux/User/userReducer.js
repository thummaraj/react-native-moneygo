import { GET_USER } from "./action";

export default (state = {}, action)=> {
    switch(action.type){
        case GET_USER:
        console.log('userReducer-check data into app', action)
        return { 
            email: action.payload.username,
            firstName: action.payload.firstName,
            lastName: action.payload.lastName,
            token: action.payload.token
         }
        default:
        return state
    }
}