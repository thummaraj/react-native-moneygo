import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import userReducer from './User/userReducer'
import walletReducer from './Wallet/walletReducer';
import selectReducer from './Wallet/selectReducer'
import incomeReducer from './Wallet/incomeReducer';

export default (history) => combineReducers({
    user: userReducer,
    wallet: walletReducer,
    select: selectReducer,
    budget: incomeReducer,
    router: connectRouter(history)
})