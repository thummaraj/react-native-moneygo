import { ADD_WALLET, UPDATE_WALLET } from "./action";

const walletReducer = (state={}, action) => {
    switch(action.type){
        case ADD_WALLET:
        console.log('addWallet', action)
        return {
            name: action.name,
            money: action.money
        }
        case UPDATE_WALLET:
        console.log('update', action)
        return {}
        default:
        return state
}
}
export default (state=[],action) =>{
    switch(action.type){
        case ADD_WALLET:
        return [...state, walletReducer(null,action)]
        default:
        return state
    }
}