import { SELECT_WALLET} from './action'

export default (state={}, action) => {
    switch(action.type){
        case SELECT_WALLET:
        console.log('selectReducer-check item', action)
        return {
            name: action.item.name,
            money: action.item.money
        }
        default: 
        return state
    }
}