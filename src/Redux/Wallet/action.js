export const ADD_WALLET = 'ADD_WALLET'
export const SELECT_WALLET = 'SELECT_WALLET'
export const GET_INCOME = 'GET_INCOME'
export const UPDATE_WALLET = 'UPDATE_WALLET'
export const GET_EXPENSE = 'GET_EXPENSE'
export const DELETE_WALLET = 'DELETE_WALLET'


export const createWallet = (name, money) => ({
    type: ADD_WALLET,
    name,
    money
})

export const selectWallet = (item) => ({
    type: SELECT_WALLET,
    item,
})

export const getIncome = (income,category) => ({
    type: GET_INCOME,
    income,
    category,
})

export const getExpense = (expense,category) => ({
    type: GET_EXPENSE,
    expense,
    category,
})

export const deleteWallet = () => ({
    type: DELETE_WALLET,
})


