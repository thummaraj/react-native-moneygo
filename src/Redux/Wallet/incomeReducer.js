import {GET_INCOME, GET_EXPENSE} from './action'

const incomeReducer = (state={}, action) => {
    switch(action.type){
        case GET_INCOME:
        console.log('incomeReducer', action)
        return {
            income: action.income,
            category: action.category
        }
        case GET_EXPENSE:
        console.log('incomeReducer EXPENSE', action)
        return {
            expense: action.expense,
            category: action.category
        }
        default:
        return state
    }
}

export default (state=[], action) => {
    switch(action.type){
        case GET_INCOME:
        console.log('Array getIncome', action.income)
        return [...state, incomeReducer(null,action)]
        case GET_EXPENSE:
        console.log('Array getExpense', action.expense)
        return [...state, incomeReducer(null,action)]
        default:
        return state
    }
}

