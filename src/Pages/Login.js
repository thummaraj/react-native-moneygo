import React, { Component } from 'react'
import { View, TextInput , ImageBackground, StyleSheet} from 'react-native'
import { Button, InputItem , WhiteSpace} from '@ant-design/react-native'
import {Icon} from 'react-native-elements'
import axios from 'axios'
import { getUser, GET_USER } from '../Redux/User/action'
import { connect } from 'react-redux'

class Login extends Component {
    state ={
        username: 'jaja@gmail.com',
        password: '12345',
        firstName: '',
        lastName: '',
        token:'',
        isAuthen: false
    }
    onLogin= ()=> {
        const { username, password} = this.state
        if(!username && !password){
            alert('Please input your email and password')
        } else
        if(!username){
            alert('Please input your email')
        } else if(!password){
            alert('Please input your password')
        } 
        else {
            axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.username,
                password: this.state.password,
            })
            .then((response) => {
                console.log('Login-check response', response)
                this.setState({
                    email: response.data.user.username,
                    firstName: response.data.user.firstName,
                    lastName: response.data.user.lastName,
                    token: response.data.user.token
                })
                this.setState({ isAuthen: true})    
                    console.log('Login-getUser from login', this.state)
                    this.props.getUser(this.state)
                    this.props.history.push('/main')
            })
            .catch((error) =>{
                if(error.response.data.errors.email === 'is invalid'){
                    alert('Your email is incorrect')
                }
                if(error.response.data.errors.password === 'is invalid'){
                    alert('Your password is incorrect')
            }
            })
        }
    }
    onRegister= ()=> {
        this.props.history.push('/register')
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <ImageBackground source={require('../assets/newOrange.png')} style={{ width: '100%', height: '100%'}}>
                
                <View style={styles.content}>
                <InputItem 
                clear
            value={this.state.username.toLowerCase()}
            onChange={value => {
              this.setState({username: value});
            }}placeholder="username">
            <Icon name='person'/>
          </InputItem>
          
          <WhiteSpace />
          <InputItem 
                clear
                type="password"
            value={this.state.password}
            onChange={value => {
              this.setState({password: value});
            }}placeholder="password">
            <Icon name='lock'/>
          </InputItem>
          <WhiteSpace />
          <View style={{  flexDirection: 'row'}}>
          <Button style={{margin: 20 , width: 110}} type="primary" onPress={this.onLogin} >Login</Button>
          <Button style={{margin: 20, width: 110}}  onPress={this.onRegister}>Register</Button>
          
          </View>
          </View>
          </ImageBackground>
            </View>
        )
    }
}
const styles = StyleSheet.create({
content: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
    
},
})
// const mapDispatchToProps = (dispatch) => {
//     return {
//         getUser: (payload) =>{
//             dispatch({
//                 type: GET_USER,
//                 payload
//         })
//     }
// }
// }
export default connect(null, {getUser})(Login)