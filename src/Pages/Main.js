import React, {Component} from 'react'
import {View, Text,StyleSheet,FlatList, TouchableOpacity,ImageBackground} from 'react-native'
import {Button, TabBar} from '@ant-design/react-native'
import { Icon } from 'react-native-elements'
import {connect} from 'react-redux'
import { selectWallet } from '../Redux/Wallet/action'


class Main extends Component {
    state={
    }    
    onGoToYourWallet=(item)=>{
        console.log('Main- select wallet', item)
        this.props.selectWallet(item)
        this.props.history.push('/yourwallet')
    }
    renderItem = ({item}) => {
        return (
            <TouchableOpacity >
          <Button onPress={() => {this.onGoToYourWallet(item)}} style={{width: '100%'}}>{item.name}: your budget = {item.money}</Button>
      </TouchableOpacity>
        )
        }
      onAddWallet=()=> {
        this.props.history.push('/addwallet')
      }
    render() {
        return(
            <View style={{flex:1}}>
             

            <ImageBackground source={require('../assets/newOrange.png')} style={{ width: '100%', height: '100%'}}>
            <Button onPress={this.onAddWallet}>+ Add Wallet</Button>
            <View style={styles.content}>
            <Text>{console.log('Main-array wallet', this.props.wallet)}</Text>
            <FlatList
        data={this.props.wallet}
        extraData={this.state}
        keyExtractor={(item) => item.id}
        renderItem={this.renderItem}
      />
      </View>
      <View style={styles.column}>
      <View style={styles.footer2} >
      <TouchableOpacity onPress={() => this.props.history.push('/main')}>
           <Icon name='home' />
           <Text style={styles.footerText}>Home</Text>
      </TouchableOpacity>
      </View>
           <View style={styles.footer2} >
           <TouchableOpacity onPress={() => this.props.history.push('/profile')}>
           <Icon  name='person' />
           <Text style={styles.footerText}>Profile</Text>
           </TouchableOpacity>
           </View>
           <View style={styles.footer2} >
           <TouchableOpacity onPress={() =>this.props.history.replace('/login')}>
           <Icon  name='power-settings-new' />
           <Text style={styles.footerText}>Logout</Text>
           </TouchableOpacity >
           </View>
      </View>
            
    
      </ImageBackground>
            </View>
            
        )
    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        
    },
    textContent: {
        fontSize: 22,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    column:{
        flexDirection: 'row',
    },
    footer2: {
        flex:1,
        alignItems: 'center',
       
    },
})
export default connect(
    ({user, wallet}) => ({user, wallet }), 
    {selectWallet}
    )(Main)