import React, { Component} from 'react'
import {View, Text, StyleSheet, ImageBackground,TouchableOpacity} from 'react-native'
import {Button, TabBar} from '@ant-design/react-native'
import { Icon } from 'react-native-elements'

class History extends Component {
    state={
        selectedTab: 'history',
    }
      
    render(){
        return(
            <View style={{flex:1}}>
            <ImageBackground source={require('../assets/som.jpg')} style={{ width: '100%', height: '100%'}}>
            
            <View style={styles.content}>
            <Text style={styles.textContent}>History</Text>
            </View>

            <View style={styles.column}>
      <View style={styles.footer2} >
      <TouchableOpacity onPress={() => this.props.history.push('/main')}>
      <Icon name='home' />
           <Text style={styles.footerText}>Home</Text>
      </TouchableOpacity>
      </View>
           <View style={styles.footer2} >
           <TouchableOpacity onPress={() => this.props.history.push('/profile')}>
           <Icon  name='person' />
           <Text style={styles.footerText}>Profile</Text>
           </TouchableOpacity>
           </View>
           <View style={styles.footer2} >
           <TouchableOpacity onPress={() =>this.props.history.push('/history')}>
           <Icon  name='restore' />
           <Text style={styles.footerText}>History</Text>
           </TouchableOpacity >
           </View>
      </View>
            </ImageBackground>
            </View>
    )

    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    textContent: {
        fontSize: 22,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    column:{
        flexDirection: 'row',
    },
    footer2: {
        flex:1,
        alignItems: 'center',
        padding: 16,
       
    },
})
export default History