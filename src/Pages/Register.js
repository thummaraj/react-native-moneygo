import React, { Component} from 'react'
import { View, Text, StyleSheet, ImageBackground} from 'react-native'
import { Button, InputItem , WhiteSpace} from '@ant-design/react-native'
import {Icon} from 'react-native-elements'
import axios from 'axios'
import {Link} from 'react-router-native'

class Register extends Component {
    state = {
        username: '',
        password: '',
        firstName: '',
        lastName: '',
        confirmPassword: ''
    }
    onSubmit=()=> {
      const { username, password, firstName,lastName,confirmPassword} = this.state
      if(!username){
        alert('Please input your email')
      } else if(!password){
        alert('Please input your password')
      }else if(!confirmPassword){
        alert('Please confirm your password')
      }else if(!firstName){
        alert('Please input your firstname')
      }else if(!lastName){
        alert('Please input your lastname')
      } else {
        axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
          email: this.state.username,
          password: this.state.password,
          firstName: this.state.firstName,
          lastName: this.state.lastName
        })
        .then((response) => {
          console.log('check register', response)
          this.props.history.push('/login')
        })
        .catch((error) => {
          console.log('check error', error)
        })
      }
    }
    render() {
        return(
            <View style={{flex: 1}}>
            <ImageBackground source={require('../assets/myApp.png')} style={{ width: '100%', height: '100%'}}>
            <View style={styles.column}>
            <View style={styles.footer1}>
            <Link to="/login">
            <Icon name='keyboard-backspace'/>
           </Link>
           </View>
           <View style={styles.footer2}>
           <Text style={styles.footerText}>Register</Text>
           </View>
           </View>
            <View style={styles.content}>
                 <InputItem 
                clear
            value={this.state.username.toLowerCase()}
            onChange={value => {
              this.setState({username: value});
            }}placeholder="username">
            <Icon name='person'/>
          </InputItem>
          <WhiteSpace />
                 <InputItem 
                clear
                type="password"
            value={this.state.password}
            onChange={value => {
              this.setState({password: value});
            }}placeholder="password">
            <Icon name='lock'/>
          </InputItem>
          <WhiteSpace />
                 <InputItem 
                clear
                type="password"
            value={this.state.confirmPassword}
            onChange={value => {
              this.setState({confirmPassword: value});
            }}placeholder="confirm password">
            <Icon name='lock'/>
          </InputItem>
          <WhiteSpace/>
          <InputItem 
                clear
            value={this.state.firstName}
            onChange={value => {
              this.setState({firstName: value});
            }}placeholder="firstname">
            <Icon name='person'/>
          </InputItem>
          <WhiteSpace/>
          <InputItem 
                clear
            value={this.state.lastName}
            onChange={value => {
              this.setState({lastName: value});
            }}placeholder="lastname">
            <Icon name='person'/>
          </InputItem>
          <WhiteSpace />
          <Button type="primary" onPress={this.onSubmit}>Submit Register</Button>
          </View>
          </ImageBackground>
            </View>
        )
    }
}
const styles = StyleSheet.create({
  content: {
      flex:1,
      alignItems: 'center',
      justifyContent: 'center'
      
  },
  column: {
    flexDirection: 'row',
    backgroundColor: 'white'
    
},
footer1: {
    flex: 1,
    padding: 16,
    
},
footer2: {
    flex:3,
    alignItems: 'center',
    padding: 16,
   
},
footerText: {
  color: 'black',
  fontSize: 18
}
  })
export default Register