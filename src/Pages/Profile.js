import React, { Component} from 'react'
import {View, Text, StyleSheet, ImageBackground, TouchableOpacity,Image} from 'react-native'
import {Button, TabBar} from '@ant-design/react-native'
import ImagePicker from 'react-native-image-picker';
import axios from 'axios'
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux'

class Profile extends Component {
    state={
        selectedTab: 'profile',
        imageURI: '',
        uri: '',
    }
    selectImage = () => {
        ImagePicker.showImagePicker({}, (response) => {
            console.log('Response = ', response);
            if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                this.setState({
                    imageURI: response.uri,
                  });
                axios.post('https://zenon.onthewifi.com/moneyGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${this.props.user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                .then(response => {
                        this.setState({
                            imageURI: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })
                }
            })
        }
    render(){
        return(
            <View style={{flex:1}}>
              <ImageBackground source={require('../assets/newOrange.png')} style={{ width: '100%', height: '100%'}}>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text>Put your Image Profile</Text>
            <Image style={[styles.circle,styles.center]}
                    source={{ uri: this.state.imageURI}}/>
                    <Button style={{width: 300}} onPress={this.selectImage}>selectImage</Button>
                    </View>
            <View style={styles.content}>
            <Text>Username : {this.props.user.email}</Text>
            <Text>Firstname : {this.props.user.firstName}</Text>
            <Text>Lastname : {this.props.user.lastName}</Text>
            </View>
            <View style={styles.column}>
      <View style={styles.footer2} >
      <TouchableOpacity onPress={() => this.props.history.push('/main')}>
      <Icon name='home' />
           <Text style={styles.footerText}>Home</Text>
      </TouchableOpacity>
      </View>
           <View style={styles.footer2} >
           <TouchableOpacity onPress={() => this.props.history.push('/profile')}>
           <Icon  name='person' />
           <Text style={styles.footerText}>Profile</Text>
           </TouchableOpacity>
           </View>
           <View style={styles.footer2} >
           <TouchableOpacity onPress={() =>this.props.history.replace('/login')}>
           <Icon  name='power-settings-new' />
           <Text style={styles.footerText}>Logout</Text>
           </TouchableOpacity >
           </View>
      </View>
      </ImageBackground>
            </View>
    )

    }
}
const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    textContent: {
        fontSize: 22,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    column:{
        flexDirection: 'row',
    },
    footer2: {
        flex:1,
        alignItems: 'center',
        padding: 16,
       
    },
    circle: {
        width: 200,
        height: 200,
        borderRadius: 200/2,
        marginLeft: 20
        
    },
})
// const mapStateToProps = (state) => {
//     return {
//         email: state.username,
//         firstName: state.firstName,
//         lastName: state.lastName,
//         token: state.token
//     }
// }
export default connect((user) => (user))(Profile)