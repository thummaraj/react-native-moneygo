import React,{Component} from 'react'
import {View, Text ,StyleSheet, TouchableOpacity, FlatList, ImageBackground} from 'react-native'
import {Icon} from 'react-native-elements'
import {Link} from 'react-router-native'
import { Button } from '@ant-design/react-native';
import { connect} from 'react-redux'

class YourWallet extends Component {
    renderItem = ({item}) => {
        if(item.income && item.category){
                return (
                    <TouchableOpacity>
          <Text style={{width: '100%'}}>Income: {item.income} category: {item.category}</Text>
      </TouchableOpacity>
                )
        } else{
            return (
                <TouchableOpacity>
              <Text style={{width: '100%'}}>Expense: {item.expense} category: {item.category}</Text>
          </TouchableOpacity>
            )
        }
    }
    onDelete=()=> {
        
    }
    onClickIncome=()=> {
        this.props.history.push('/income')
    }
    onClickExpense=()=>{
        this.props.history.push('/expense')
    }
    render(){
        return(
            <View style={{flex:1}}>
            <ImageBackground source={require('../assets/newOrange.png')} style={{ width: '100%', height: '100%'}}>
            <View style={styles.column}>
            <View style={styles.footer1}>
            <Link to="/main">
            <Icon name='keyboard-backspace'/>
           </Link>
           </View>
           <View style={styles.footer2}>
           <Text style={styles.footerText}>{this.props.select.name}</Text>
           
           </View>
           <TouchableOpacity  onPress={this.onDelete}>
           <View style={styles.footer2}>
           <Text style={styles.footerText}>Delete</Text>
           </View>
           </TouchableOpacity>
           </View>

           <View style={styles.content}>
           <Button>{this.props.select.money}</Button>
           <FlatList
        data={this.props.budget}
        extraData={this.state}
        keyExtractor={(item) => item.id}
        renderItem={this.renderItem}
      />

           </View>
           <View style={styles.column}>
           <View style={styles.footer1}>
           <Icon name='add-circle-outline' style={{width: 100, height: 100}}onPress={this.onClickIncome}>Income</Icon>
           </View>
           <View style={styles.footer1}>
           <Icon name='remove-circle-outline' onPress={this.onClickExpense}>Expense</Icon>
           </View>
           </View>
           </ImageBackground>
           </View>
        )
    }
}
const styles = StyleSheet.create({
    column: {
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'center'
        
    },
    footer1: {
        flex: 1,
        padding: 16,
        
    },
    footer2: {
        flex:3,
        alignItems: 'center',
        padding: 16,
       
    },
    footerText: {
      color: 'black',
    },
    content: {
        backgroundColor: 'white',
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    textContent: {
        fontSize: 22,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
        width: 120,
        height: 120,
        borderRadius: 120/2,
        marginLeft: 20
        
    },
    circle2: {
        width: 120,
        height: 120,
        borderRadius: 120/2,
    },
})
export default connect(({select, budget}) => ({select, budget}))(YourWallet)