import React, { Component} from 'react'
import { View , Text, StyleSheet} from 'react-native'
import { Link} from 'react-router-native'
import { Icon } from 'react-native-elements'
import { Button } from '@ant-design/react-native';
 
class EditWallet extends Component {
    render() {
        return(
            <View style={{flex:1}}>
                <View style={styles.column}>
            <View style={styles.footer1}>
            <Link to="/yourwallet">
            <Icon name='keyboard-backspace'/>
           </Link>
           </View>
           <View style={styles.footer2}>
           <Icon name='done'/>
           </View>
           </View>

           <View style={styles.content}>
           <Button>Edit</Button>
           <Button>Delete wallet</Button>
           </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    content: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    column: {
      flexDirection: 'row',
      backgroundColor: 'white'
      
  },
  footer1: {
      flex: 1,
      padding: 16,
      
  },
  footer2: {
      flex:4,
      alignItems: 'center',
      padding: 16,
      marginLeft: 200
     
  },
  footerText: {
    color: 'black',
    fontSize: 18
  }
    })
export default EditWallet