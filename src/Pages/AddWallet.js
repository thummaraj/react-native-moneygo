import React, {Component } from 'react'
import {View, Text, StyleSheet,TouchableOpacity,ImageBackground} from 'react-native'
import {Link} from 'react-router-native'
import { InputItem, Button, WhiteSpace } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements';
import { createWallet} from '../Redux/Wallet/action'


class AddWallet extends Component {
    state = {
        name: '',
        money: 0.0
    }
    onCreateWallet=()=> {
        const { name, money } = this.state
        if(!name && !money){
            alert('Please input your wallet name and money')
        } else if(!name){
            alert('Please input your wallet name')
        } else if(!money){
            alert('Please input your money')
        } else{
        this.props.createWallet(name,money)
        this.props.history.push('/main')
    }
}
    render(){
        return(
            <View style={{flex:1}}>
            <ImageBackground source={require('../assets/newOrange.png')} style={{ width: '100%', height: '100%'}}>
             <View style={styles.column}>
             <View style={styles.footer1}>
             <Link to="/main">
             <Icon name='keyboard-backspace'/>
            </Link>
            </View>
            <View style={styles.footer2}>
            <Text style={styles.footerText}>My Wallet</Text>
            </View>
            </View>

            <View style={styles.content}>
            <Text style={styles.textContent}>Create Wallet</Text>
            <InputItem 
            placeholder="name"
            type="name"
            value={this.state.name}
            onChange={value => {this.setState({ name: value })}}/>
            <InputItem 
            placeholder="money"
            type="number"
            value={this.state.money}
            onChange={value => {this.setState({ money: value })}}/>
            <WhiteSpace/>
            <Button onPress={this.onCreateWallet} type="primary">create</Button>
            </View>
            </ImageBackground>
             </View>
        )
    }
}
const styles = StyleSheet.create({
    column: {
        flexDirection: 'row',
        backgroundColor: 'white'
    },
    footer1: {
        flex: 1,
    padding: 16,
        
    },
    footer2: {
        flex:3,
        alignItems: 'center',
        padding: 16,
       
    },
    footerText: {
        fontSize: 18,
        color: 'black',
        
    },
    content: {
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    textContent: {
        fontSize: 22,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    }
})
export default connect(null, { createWallet})(AddWallet)
