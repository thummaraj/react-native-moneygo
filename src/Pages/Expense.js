import React, { Component} from 'react'
import { View , Text,StyleSheet, ImageBackground,TouchableOpacity} from 'react-native'
import {InputItem,Button} from '@ant-design/react-native'
import { Icon} from 'react-native-elements'
import {Link} from 'react-router-native'
import { getExpense} from '../Redux/Wallet/action'
import {connect} from 'react-redux'
class Expense extends Component {
    state = {
        expense: 0.0,
        category: ''
    }
    onaddExpense=()=>{
        const { expense, category } = this.state
        if(!expense && !category){
            alert('Please input your expense and category')
        } else if(!expense){
            alert('Please input your expense')
        } else if(!category){
            alert('Please input category')
        } else{
        this.props.getExpense(expense,category)
        this.props.history.push('/yourwallet')
    }
    }
    render() {
        return(
            <View style={{flex:1}}>
            <ImageBackground source={require('../assets/newOrange.png')} style={{ width: '100%', height: '100%'}}>
            <View style={styles.column}>
            <View style={styles.footer1}>
            <Link to="/yourwallet">
            <Icon name='keyboard-backspace'/>
           </Link>
           </View>
           <View style={styles.footer2}>
           <Text style={styles.footerText}>Expense</Text>
           </View>
           <TouchableOpacity onPress={this.onaddExpense}>
           <View style={styles.footer2}>
           <Icon name='done'/>
           </View>
           </TouchableOpacity>
           </View>
           <View style={styles.content}>
           <InputItem  clear
                type="number"
            value={this.state.expense}
            onChange={value => {
              this.setState({expense: value});
            }}placeholder="0.00"/>
              <InputItem  clear
                type="name"
            value={this.state.category}
            onChange={value => {
              this.setState({category: value});
            }}placeholder="caterory"/>
           </View>
           </ImageBackground>
           </View>
        )
    }
}
const styles = StyleSheet.create({
    column: {
        flexDirection: 'row',
        backgroundColor: 'white'
        
    },
    footer1: {
        flex: 1,
        padding: 16,
        
    },
    footer2: {
        flex:4,
        alignItems: 'center',
        padding: 16,
        
       
    },
    footerText1: {
        fontSize: 22,
        color: 'gray',
        margin: 10
    },
    content: {
        backgroundColor: 'white',
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
    textContent: {
        fontSize: 22,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        backgroundColor: 'white',
        flex: 3,
        alignItems: 'center',
        justifyContent: 'center'
        
    },
})
export default connect(null,{getExpense})(Expense)