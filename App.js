
import React, {Component} from 'react';
import { Provider } from 'react-redux'
import Router from './src/Router/Router'
import { createMemoryHistory} from 'history'
import createStore from './Store'
import { NativeRouter} from 'react-router-native'



export default class App extends Component  {
  history = createMemoryHistory()
  store = createStore(this.history)
  render() {
    return (
      <Provider store={this.store}>
         <Router history={this.history}/>
      </Provider>
    );
  }
}