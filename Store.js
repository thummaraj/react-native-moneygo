import { compose, createStore, applyMiddleware } from "redux";
import { routerMiddleware } from "connected-react-router";
import reducer from './src/Redux'


// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// export const history = createMemoryHistory()
// export const store = createStore(
//     reducer(history),
//     composeEnhancers(
//         applyMiddleware(
//             routerMiddleware(history),
//             logger,
//         )
//     )
// )
export default (history) => {
    return createStore(
        reducer(history),
        undefined,
        compose(
            applyMiddleware(
                routerMiddleware(history)
            )
        )
    )
}


